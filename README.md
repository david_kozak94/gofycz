# README #

This repository contains source code for a very simple android app __Get Out Of Your Comfort Zone__. The mission of this app is to challenge you every day with a creative tasks that should be unusual and unexpected. Therefore you should be challenged every day.

The app can be downloaded from [google play](https://play.google.com/store/apps/details?id=io.dkozak.gofycz) 

### What is this repository for? ###

* Get out of your comfort zone android app
* Version 1.1

### How do I get set up? ###

* Simply clone the repo

### Who do I talk to? ###

* dkozak94@gmail.com