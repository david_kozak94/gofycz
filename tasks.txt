Zastav se u pouličního umělce. Ideálně se s ním i seznam.
Choď celý den výstředně oblečený.
Dej se do hovoru s náhodným člověkem. 
Naprav nějakou chybu z minulosti, která tě mrzí.
Usmiř se s některým ze svých 'nepřátel'.
Zavolej kamarádovi či kamarádce, se kterým/kterou ses dlouho neviděl, a domluv si schůzku.
Drž alespoň půl dne půst.
Jdi na procházku do lesa.
Projdi své obydlí a zbav se všeho, co už nepotřebuješ. 
Udělej si z dneška rodinný den. Kontaktuj všechny členy rodiny, co tě napadnou, a popovídej si s nimi.
Jdi se v noci projít při měsíci.
Přeprav se někam stopem.
Naplánuj si na nejbližší možný termín originální výlet.
Jdi se proběhnout do přírody.
Prožij celý den bez mobilního telefonu.
Napiš dnes básničku. 
Dnes namaluj obrázek.
Vyber si jednu z věcí, které se bojíš, a zrealizuj ji.
Obejmi dnes alespoň 5 lidí. 
Dnes se neukazuj na žádné sociální síti. Místo toho si povídej s lidmi kolem tebe.
Během dne si uvědom alespoň 10 věcí, za které jsi vděčný/vděčná. Sepiš si je na papír.
Jdi do antikvariátu a kup si knížku, která tě zaujme. Potom si jí samozřejmě přečti.
Dnes řekni každému, s kým se budeš mluvit, jednu pozitivní věc, která se ti na ní/něm líbí.
Zrealizuj některý z nápadů, se kterými si pohráváš ve své hlavě. 
Stanov si dnes nový cíl podle metody SMART.
Nauč se dnes nějaký nový vtip. Pak ho vyprávěj lidem na potkání.
Vydej se dnes na náhodný výlet. Jdi na nádraží a jeť prvním spojem, který se naskytne. Vystup tam, kde se ti bude nejvíce líbít.
Vyber si nějakou věc, kterou jsi dříve nepochopil/nepochopila. Dnes se do ní s chutí znovu pusť. Určitě to zvládneš :)
Dej dnes někomu náhodně vybraný vtipný dárek. 
Dej dnes někomu květiny. 
Otevři jednu z knih, kterou jsi začal číst, ale nedočetl. Pokud tě zaujme, dočti ji. Jinak asi nebude pro tebe, zkus ji někomu půjčit či nabídnout jako dárek.

Najdi a přihlaš se na přednášku o něčem, co tě baví.
Projdi si seznam svých životních cílů a zamysli se, zda zdále platí. Pokud takový seznam ještě nemáš, vytvoř si ho.
Dnes udělej nějakou věc, kterou by do tebe nikdo neřekl.
Rozhoduj se celý den spontánně.
Choď alespoň část dne bosý/bosá.
Zamysli se a vzpomeň na cíl, kterého jsi kdysi chtěl dosáhnout, ale zazdil jsi to. Máš ho? Ode dneška na něm začni pracovat :)
Přidej si nový obrázek na svoji motivační nástěnku. Pokud ji ještě nemáš, vytvoř si ji.
Dnes dej všude spropitné. 
Zavzpomínej na dobré časy s tvými nejlepšími přáteli. Pak každého kontaktuj a poděkuj jim za ně.
Vyber si jednu dovednost, se kterou u sebe nejsi spokojený. Pokud je pro tebe důležitá, dnes se v ní překonej. Posuň svůj limit(Třeba také časem zjistíš, že žádný není :) ). Pokud pro tebe daná věc důležitá není, přestaň se jí alespoň trápit.
Během dne udělej alespoň 5 dobrých skutků.
Máš ve svém okolí nějaké místo, které by mohlo být krásné, ale je zaneřáděné odpadky? Určitě se nějaké najde. Dnes si najdi čas a vyčisti ho.
Promluv si dnes s někým cizí řečí.
Otevři svůj telefonní seznam, zavři oči a naprosto náhodně někomu zavolej. S danou osobou si popovídej, ideálně i domluv schůzku. Pokud jsi měl/měla to štěstí trefit infolinku či něco podobného, zkus to znovu :)
Sepiš si seznam nejkrásnějších zážitků tvého dosavadního života a poděkuj za ně. Není to krása? Až to budeš mít, někam viditelně si ho vylep.
Kup si nějakou knížku o osobním rozvoji a s chutí se pusť do čtení.
Dnes jez celý den zdravě.
Vyber si město ve tvém okolí, kde jsi ještě nebyl/nebyla. Zajeď si tam na výlet.
Jdi se projít po pro tebe neznámých částech tvého města a objevuj nová místa.
Dneska prožij alespoň tři hodiny pod širým nebem.
Přečti si životní příběh některého z lidí, ke kterým vzhlížíš. Pokud žádného nemáš, zkus si nějakého najít.
Dnes jdi na celkem dlouhý (například více než 10km) pěší výlet. Nebo proveď nějakou podobnou pro tebe nestandardní fyzicky náročnou činnost.
Vyhraď si dnes čas a pozoruj krásu západu Slunce.
Dneska si pouštěj jen a pouze svoje nejoblíbenější písničky, které rád posloucháš, když jdi šťastný/šťastná. Pusť si jich alespoň 10.
Vyber si nějaký ze svých zlozvyků, kterých by ses rád zbavil. Dneska ho vůbec nedělej a stanov si plán, jako ho odstranit trvale.
Napiš někomu ze svých nejlepších dopis či pohled. Jen tak. Vyjádři v něm, jak ti na nich záleží :)
Předávej během dne anonymně lidem pozitivní vzkazy. Sleduj jejich reakce, až je uvidí.
Dnes prolom některý ze svých dosavadních fyzických limitů. Například počet kliků, dřepů, shybů, maximálku na bench pressu či počet uběhnutých kilometrů.
Skonči dneska brzy v práci/se školou, vypni si mobil a s strav celý večer ve společnosti lidí, co máš rád/ráda.
Každý máme svůj seznam věcí, co už dlouhodobě odkládáme. Ale notak, alespoň pár se určitě najde. Dneska jich co nejvíc udělej a ty, co udělat hned nemůžeš, si závazně naplánuj.
Dneska naplánuj příjemné překvapení pro někoho ze svých blízkých.

