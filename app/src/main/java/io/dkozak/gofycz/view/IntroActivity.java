package io.dkozak.gofycz.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

import io.dkozak.gofycz.R;
import io.dkozak.gofycz.utils.ExceptionHandler;

/**
 * Introduction  - few slides
 * run only once when the app starts
 */

public class IntroActivity extends AppIntro {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//


        // set exception handler that prints info to log
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());


        addSlide(AppIntroFragment.newInstance(getString(R.string.intro_title_1), getString(R.string.intro_text_1), R.drawable.ic_done, Color.parseColor("#0099cc")));
        addSlide(AppIntroFragment.newInstance(getString(R.string.intro_title_2), getString(R.string.intro_text_2), R.drawable.ic_done, Color.parseColor("#0099cc")));
        addSlide(AppIntroFragment.newInstance(getString(R.string.intro_title_3), getString(R.string.intro_text_3), R.drawable.ic_done, Color.parseColor("#0099cc")));
        addSlide(AppIntroFragment.newInstance(getString(R.string.intro_title_4), getString(R.string.intro_text_4), R.drawable.ic_done, Color.parseColor("#0099cc")));

    }

    @Override
    public void onDonePressed() {
        super.onDonePressed();

        Intent i = new Intent(this,
                CurrentTaskActivity.class);
        startActivity(i);
        finish();

    }

    @Override
    public void onSkipPressed() {
        super.onSkipPressed();
        Intent i = new Intent(this,
                CurrentTaskActivity.class);
        startActivity(i);
        finish();
    }


}
