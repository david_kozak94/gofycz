package io.dkozak.gofycz.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import io.dkozak.gofycz.Controller;
import io.dkozak.gofycz.R;
import io.dkozak.gofycz.model.Category;
import io.dkozak.gofycz.model.Task;
import io.dkozak.gofycz.utils.ExceptionHandler;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class CurrentTaskActivity extends AppCompatActivity {

    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());


        //make it fullscreen
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_current_task);

        final Controller controller = Controller.getInstance();
        final TextView taskView = (TextView) findViewById(R.id.fullscreen_content);
        final ImageView confirmButton = (ImageView) findViewById(R.id.finished_button);


        try {
            Task task = controller.getCurrentTask(this);
            initViewForActiveTask(task, taskView, confirmButton);

        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
            Toast.makeText(this, R.string.failed_to_load_task, Toast.LENGTH_LONG).show();
        }
    }


    private void initViewForActiveTask(final Task task, final TextView taskView, final ImageView confirmButton) {
        taskView.setText(task.getText());
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counter++;
                if (counter == 3) {
                    counter = 0;
                    Controller controller = Controller.getInstance();
                    controller.markTaskAs(task, Category.DONE);
                    try {
                        Task currentTask = controller.getCurrentTask(CurrentTaskActivity.this);
                        initViewForActiveTask(currentTask, taskView, confirmButton);
                    } catch (IllegalAccessException ex) {
                        ex.printStackTrace();
                        Toast.makeText(CurrentTaskActivity.this, R.string.failed_to_load_task, Toast.LENGTH_LONG).show();
                    }
                } else if (counter == 1) {
                    Toast.makeText(CurrentTaskActivity.this, R.string.two_more_times, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
