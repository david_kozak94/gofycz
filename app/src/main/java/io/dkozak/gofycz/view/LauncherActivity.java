package io.dkozak.gofycz.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import io.dkozak.gofycz.Controller;
import io.dkozak.gofycz.R;
import io.dkozak.gofycz.utils.ExceptionHandler;

/**
 * Activity that simply shows the logo and title, waits for three seconds and starts the MainPage
 */
public class LauncherActivity extends AppCompatActivity {
    public static final int LOADING_TIME_IN_SECONDS = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_launcher);
        final Controller controller = Controller.getInstance();

        // set exception handler that prints info to log
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());

        Thread welcomeThread = new Thread() {

            @Override
            public void run() {
                boolean firstTime = false;
                try {
                    long timeStart = System.currentTimeMillis();
                    if (getPreferences(MODE_PRIVATE).getBoolean("is_first_run", true)) {
                        firstTime = true;
                        // run onlygetDeclaredFields()the database
                        try {
                            controller.initDatabase(LauncherActivity.this);
                        } catch (IllegalAccessException ex) {
                            ex.printStackTrace();
                            Toast.makeText(LauncherActivity.this, "Failed to init database", Toast.LENGTH_LONG).show();
                        }
                        getPreferences(MODE_PRIVATE).edit().putBoolean("is_first_run", false).commit();
                    }

                    super.run();
                    sleep((LOADING_TIME_IN_SECONDS * 1000) - (System.currentTimeMillis() - timeStart));
                    //Delay for specified time minus the data loading time
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    Intent i;
                    if (!firstTime)
                        i = new Intent(LauncherActivity.this,
                                CurrentTaskActivity.class);
                    else
                        i = new Intent(LauncherActivity.this
                                , IntroActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        };
        welcomeThread.start();
    }
}
