package io.dkozak.gofycz;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Random;

import io.dkozak.gofycz.model.Category;
import io.dkozak.gofycz.model.Task;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Loads data and controlls views
 */
public class Controller {

    public static final boolean DEBUG = false;
    private static final Controller instance = new Controller();

    public static Controller getInstance() {
        return instance;
    }

    public Task getCurrentTask(Context context) throws IllegalAccessException {
        Realm realm = Realm.getDefaultInstance();

        RealmResults<Task> results = realm.where(Task.class).equalTo("category", Category.SELECTED.name()).findAll();
        if (results.size() > 1) {
            throw new RuntimeException("Only one task should be selected at any given time " + results.toString());
        } else if (results.size() == 1) {
            return results.get(0);
        } else
            return setupAndGetNewTask(context);
    }

    public void initDatabase(@Nullable Context context) throws IllegalAccessException {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        // just a small hack using reflection to add support for two languages dynamically
        Field[] declaredFields = R.string.class.getDeclaredFields();
        for (Field field : declaredFields) {
            if (field.getName().contains("task__")) {
                Integer description = (Integer) field.get(null);
                if (context != null && DEBUG)
                    Toast.makeText(context, description, Toast.LENGTH_LONG).show();
                Task task = realm.createObject(Task.class);
                task.setText(description);

            }

        }
        realm.commitTransaction();
    }

    private void reinitDatabase() throws IllegalAccessException {
        Realm defaultInstance = Realm.getDefaultInstance();
        defaultInstance.beginTransaction();
        defaultInstance.deleteAll();
        defaultInstance.commitTransaction();
        initDatabase(null);
    }

    private Task setupAndGetNewTask(Context context) throws IllegalAccessException {
        Random random = new Random();
        Realm realm = Realm.getDefaultInstance();

        List<Task> virginTasks = getTaskFromCategory(realm, Category.VIRGIN);
        List<Task> doneTasks = getTaskFromCategory(realm, Category.DONE);

        if (random.nextDouble() > 0.8 && doneTasks.size() > 0) {
            // repeat some older task
            return getRandomTaskAndSelectIt(random, doneTasks);
        } else if (virginTasks.size() > 0) {
            // get virgin task
            return getRandomTaskAndSelectIt(random, virginTasks);

        } else {
            Toast.makeText(context, "Congratulations, you have finished all available tasks :)", Toast.LENGTH_LONG).show();
            Toast.makeText(context, "App will restart its database, so that you can do it again :)", Toast.LENGTH_LONG).show();
            // restart database and get random virgin task
            reinitDatabase();
            return getRandomTaskAndSelectIt(random, getTaskFromCategory(realm, Category.VIRGIN));
        }
    }

    private Task getRandomTaskAndSelectIt(Random random, List<Task> doneTasks) {
        return markTaskAs(getRandomTaskFromList(doneTasks, random), Category.SELECTED);
    }

    private Task getRandomTaskFromList(List<Task> tasks, Random random) {
        return tasks.get(random.nextInt(tasks.size()));
    }

    private RealmResults<Task> getTaskFromCategory(Realm realm, Category category) {
        return realm.where(Task.class).equalTo("category", category.name()).findAll();
    }

    public Task markTaskAs(Task task, Category category) {
        Realm defaultInstance = Realm.getDefaultInstance();
        defaultInstance.beginTransaction();
        task.setCategory(category.name());
        defaultInstance.commitTransaction();
        return task;
    }


}
