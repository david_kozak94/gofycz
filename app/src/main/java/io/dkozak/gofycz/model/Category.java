package io.dkozak.gofycz.model;

/**
 * Tasks can be in different categories based on the way user last interacted with them
 */
public enum Category {
    VIRGIN, // has not been encountered yet
    SELECTED, // currently selected task
    DONE, // user has already finished this task once
}
