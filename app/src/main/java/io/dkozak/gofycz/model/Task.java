package io.dkozak.gofycz.model;


import java.util.Date;
import io.realm.RealmObject;

/**
 * One task stored in the local database
 */
public class Task extends RealmObject {
    private int text;
    private String category = Category.VIRGIN.toString();

    public Task() {
    }

    public Task(int text, Date assignedAt, String category, boolean isSelected) {
        this.text = text;
        this.category = category;
    }

    public int getText() {
        return text;
    }

    public void setText(int text) {
        this.text = text;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
